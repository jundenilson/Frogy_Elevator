﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public GameObject canvasMenu;

	public GameObject canvasRecord;
	public UnityEngine.UI.Text highScore;
	public UnityEngine.UI.Text lastScore;

	public bool recordsOn;

	// Use this for initialization
	void Start () {
		recordsOn = false;
		canvasMenu.SetActive(true);
		canvasRecord.SetActive(false);
	}

	void Update(){
		highScore.text = "High Score: " + PlayerPrefs.GetInt ("highScore");
		lastScore.text = "Last Score: " + PlayerPrefs.GetInt ("lastScore");
	}
	
	public void play(){
		Application.LoadLevel ("Infinity_Run");
	}
	public void tutorial(){
		Application.LoadLevel ("teste com trigger");
	}
	public void records(){
		canvasMenu.SetActive(recordsOn);
		canvasRecord.SetActive(!recordsOn);
		recordsOn = !recordsOn;
	}
	
	public void resetScore(){
		PlayerPrefs.SetInt ("highScore", 0);
		PlayerPrefs.SetInt ("lastScore", 0);
	}

}
