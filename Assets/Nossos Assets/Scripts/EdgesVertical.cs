﻿using UnityEngine;
using System.Collections;

public class EdgesVertical: MonoBehaviour {
    public GameObject clone;
    float cloneSize;
    public bool Top;
    bool vivo = false;

	public GameObject Controlador;

    void Start(){
        cloneSize = clone.GetComponent<Renderer>().bounds.size.y;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
	
		if ((other.tag == "Elevador-up" && Top) && other.transform.position != null)
        {
            var elevator = Instantiate(clone, new Vector2(other.transform.position.x, -(GetComponent<Transform>().position.y + cloneSize / 2)), Quaternion.identity) as GameObject;
			elevator.GetComponent<Rigidbody2D>().velocity = other.GetComponent<Rigidbody2D>().velocity;
			elevator.tag = "Elevador-up";
        }

		if((other.tag == "Elevador-down" && !Top) && other != null)
        {
            var elevator = Instantiate(clone, new Vector2(other.transform.position.x, -(GetComponent<Transform>().position.y - cloneSize / 2)), Quaternion.identity) as GameObject;
            elevator.GetComponent<Rigidbody2D>().velocity = other.GetComponent<Rigidbody2D>().velocity;
            elevator.tag = "Elevador-down";
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (((other.tag == "Elevador-up" && Top) || (other.tag == "Elevador-down" && !Top)) && other != null) {
            Destroy(other.gameObject);
        }
        if (other.tag == "Player" && !Top)
        {
            if (!vivo) vivo = true;
			else{
				PlayerPrefs.SetInt("lastScore",Controlador.GetComponent<ControladorJogo>().getScore());
				Application.LoadLevel("Parabéns");// 0 é o GAME OVER, CHORA!
			}
		}
    }
}
