﻿using UnityEngine;
using System.Collections;

public class Limbo : MonoBehaviour {

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
            Application.LoadLevel(0);
        else Destroy(other.gameObject);
    }
}
