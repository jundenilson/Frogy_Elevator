﻿using UnityEngine;
using System.Collections;

public class ElevatorSpawner : MonoBehaviour {
    public GameObject clone_cima;
    //public GameObject clone_baixo;
    public GameObject elevador;

    public Vector2 spawnRange;
    public float velocidadeTela;
    public float velocidadeElevador;
    public float space;
    private float nextElevator;
    public float Elevator_Velocity_Variador_Y;

    public void Update() {
        Spanwnador();
    }

    void Spanwnador()
    {   
        if (Time.time > nextElevator)
        {
            nextElevator = Time.time + space;
            Vector2 spawnPosition_cima = new Vector2(Random.Range((GetComponent<Transform>().position.x - spawnRange.x), (GetComponent<Transform>().position.x + spawnRange.x)), Random.Range(-spawnRange.y, spawnRange.y));
            //Vector2 spawnPosition_baixo = new Vector2(spawnPosition_cima.x, spawnPosition_cima.y - 10.0f);
            clone_cima = Instantiate(elevador, spawnPosition_cima, Quaternion.identity) as GameObject;
            //clone_baixo = Instantiate(elevador, spawnPosition_baixo, Quaternion.identity) as GameObject;
            float Variador = Random.Range(0.5f,1.5f);
            if (Random.value < 0.5){
                clone_cima.tag = "Elevador-down";
                //clone_baixo.tag = "Elevador-down";
                clone_cima.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidadeTela , -velocidadeElevador*Variador);
                //clone_baixo.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidadeTela, -velocidadeElevador * Variador);
            }
            else{
                clone_cima.tag = "Elevador-up";
                //clone_baixo.tag = "Elevador-up";
                clone_cima.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidadeTela, velocidadeElevador * Variador);
                //clone_baixo.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidadeTela, velocidadeElevador * Variador);
            }
        }
    }
    public float getVelocTela()
    {
        return velocidadeTela;
    }
}
