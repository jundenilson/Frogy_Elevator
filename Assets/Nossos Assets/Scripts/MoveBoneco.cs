﻿using UnityEngine;
using System.Collections;

public class MoveBoneco : MonoBehaviour {
    GameObject playerObj;
    static Rigidbody2D player;

    float moveSpeed = 0.0f;
    public  float m_moveSpeed = 150.0f;
    public float max_veloc = 750.0f;
    public bool olhando_direita = true; //variável para o Animador

    private Animator animador;

    //
    public GameObject botao_esquerda;
    public GameObject botao_direita;


    void Start () {
        player = GetComponent<Rigidbody2D>();
        animador = GetComponent<Animator>();
    }

	public void OnWalkDown_Dir(){
        moveSpeed = m_moveSpeed;
		animador.SetBool("Movimento", true);
        if (!olhando_direita)
            flip();
    }

    public void OnWalkDown_Esq(){
        moveSpeed = -m_moveSpeed;
		animador.SetBool("Movimento", true);
        if (olhando_direita)
            flip();
    }

    public void OnWalkUp(){
        moveSpeed = 0.0f;
		animador.SetBool("Movimento",false);
        if (olhando_direita)    player.velocity = new Vector2(0.0f, player.velocity.y);
        else player.velocity = new Vector2(0.0f, player.velocity.y * Time.deltaTime);
    }

    public void FixedUpdate()
    {
        if (player.velocity.x < max_veloc && player.velocity.x > -max_veloc) {
            player.velocity += new Vector2(Time.deltaTime*moveSpeed, 0.0f);
            //player.velocity *= moveSpeed;
        }
//        if (player.velocity.x == 0) animador.SetBool("Movimento",false);
//        else  animador.SetBool("Movimento", true);
    }

    public void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A))

        //else if (Input.GetKeyDown(KeyCode.D))
        //    OnWalkDown_Dir();
    }

    void flip()
    {
        olhando_direita = !olhando_direita;
        transform.Rotate(Vector3.up, 180.0f , Space.World);
    }
}
