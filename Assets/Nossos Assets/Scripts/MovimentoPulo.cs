﻿using UnityEngine;
using System.Collections;

public class MovimentoPulo : MonoBehaviour {
    GameObject playerObj;
    static Rigidbody2D player;
    public float jumpSpeed = 1500.0f;

	//No Chao
    public  bool noChao = false;
    public Transform groundCheck;
    public float groundRadius = 0.2f;
    public LayerMask whatIsGround;

	//Pulo Duplo
	public bool primeiroPulo= true;

    private Animator animador;

    void Awake(){
        groundCheck = GameObject.Find("GroundCheck").transform;//Coloca as coordenadas na variavel 
        animador = GetComponent<Animator>();
    }

    void Start(){
        player = GetComponent<Rigidbody2D>();
    }

    void Update(){
        noChao = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
		if (noChao)
			primeiroPulo = true;
    }

    void FixedUpdate()
    {
        //pra teste
        if (Input.GetKeyDown(KeyCode.Space))
            MoveMeUp();
        if (noChao) animador.SetBool("Solado", true);
        else animador.SetBool("Solado", false);
    }

    public void MoveMeUp(){
        if (noChao) {
			pulo ();
		} else if (primeiroPulo) {
			pulo ();
			primeiroPulo = false;
		}
        
		//else animador.SetBool("Solado", false);
    }

	void pulo(){
		player.velocity = new Vector2(player.velocity.x, 0.0f); //PAra não ter interferencia da velocidade Y dos elevadores
		
		player.AddForce(new Vector2(0, jumpSpeed));
		GetComponent<AudioSource>().Play();
	}
}







