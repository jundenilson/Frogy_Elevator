﻿using UnityEngine;
using System.Collections;

public class First_Elevator : MonoBehaviour {
    public float vel;
    public GameObject Elevator_Spawner;
	void Start () {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, vel);
        //Debug.Log(elevador.GetComponent<Renderer>().bounds.size.y);
    }
	
	void Update () {
        if (GetComponent<Rigidbody2D>().position.y >= -(5.0 - GetComponent<Renderer>().bounds.size.y /2 ))
            GetComponent<Rigidbody2D>().velocity = new Vector3(0.0f, 0.0f);
        if (GetComponent<Rigidbody2D>().velocity == Vector2.zero)
        {
           GetComponent<Rigidbody2D>().velocity = new Vector3(Elevator_Spawner.GetComponent<ElevatorSpawner>().velocidadeTela, 0.0f);
        }
    }


}
