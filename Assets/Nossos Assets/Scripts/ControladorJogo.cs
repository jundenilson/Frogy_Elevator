﻿using UnityEngine;
using System.Collections;

public class ControladorJogo : MonoBehaviour {
    static int score;
    public UnityEngine.UI.Text mostrador;
    public static  int contador;
	public GameObject canvasGameOn;
	public int scoreDelay = 11;
    
	bool PAUSED;// = false;
    public GameObject canvasPause;

    void Awake()// Antigo start()
    {
        PAUSED = false;
		if (Application.loadedLevelName == "Infinity_Run")
        {
//            score = -190;
            canvasGameOn.SetActive(false);
            canvasPause.SetActive(false);
			Debug.Log("Falsetou no Awake");
        }
        else contador += 1;

		/*if(Application.loadedLevelName == "parabéns"){
			mostrador.text ="Score: " + PlayerPrefs.GetString("lastScore");
			Debug.Log("Pegou player prefs");
		}*/
    }

    void Update()
    {
        if (Application.loadedLevelName == "Infinity_Run")
        {
            score /*+*/= (int)Mathf.Ceil(Time.timeSinceLevelLoad * Mathf.PI /* /1000 */);
            acordaCanvas();
        
			if (score > scoreDelay)
			{
				mostrador.text = "Score: " + (score - scoreDelay) /* /20 */ + "0";
				//            acordaCanvas();
			}
			else mostrador.text = "Sscore: 0";
			//        Debug.Log("Tempo: " + Time.time);
		}
	}



    public void acordaCanvas()
    {
		if (score > scoreDelay && !PAUSED)
            canvasGameOn.SetActive(true);
    }

    public int getContador()
    {
        return contador;
    }

	public int getScore(){
		return int.Parse((score - scoreDelay) + "0");
	}

    public void PauseButton()
    {
        PAUSED = !PAUSED;
		canvasGameOn.SetActive(!PAUSED);
        Time.timeScale = PAUSED ? 0 : 1;
        canvasPause.SetActive(PAUSED);
    }

	public void restart(){
		Application.LoadLevel (Application.loadedLevel);
		PAUSED = false;
		Time.timeScale = 1;
	}

	public void barraFF(){
		Application.LoadLevel("Menu");
		PAUSED = false;
		Time.timeScale = 1;
	}
}
